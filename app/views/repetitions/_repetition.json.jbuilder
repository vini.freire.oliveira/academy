json.extract! repetition, :id, :gym_id, :number, :created_at, :updated_at
json.url repetition_url(repetition, format: :json)
