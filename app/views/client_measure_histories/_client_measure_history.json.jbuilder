json.extract! client_measure_history, :id, :client_id, :measures_id, :data_measures, :created_at, :updated_at
json.url client_measure_history_url(client_measure_history, format: :json)
