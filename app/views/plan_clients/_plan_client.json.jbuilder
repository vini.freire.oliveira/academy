json.extract! plan_client, :id, :gym_id, :name, :price, :created_at, :updated_at
json.url plan_client_url(plan_client, format: :json)
