json.extract! client, :id, :user_id, :gym_id, :plan_client_id, :address, :phone, :name, :cpf, :created_at, :updated_at
json.url client_url(client, format: :json)
