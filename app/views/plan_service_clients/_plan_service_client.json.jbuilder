json.extract! plan_service_client, :id, :plan_client_id, :service_client_id, :created_at, :updated_at
json.url plan_service_client_url(plan_service_client, format: :json)
