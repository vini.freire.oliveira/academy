json.extract! training_client, :id, :client_id, :training_id, :group, :turn, :created_at, :updated_at
json.url training_client_url(training_client, format: :json)
