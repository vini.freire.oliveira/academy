json.extract! admin, :id, :user_id, :name, :created_at, :updated_at
json.url admin_url(admin, format: :json)
