json.extract! client_payment_history, :id, :client_id, :price, :payment_day, :created_at, :updated_at
json.url client_payment_history_url(client_payment_history, format: :json)
