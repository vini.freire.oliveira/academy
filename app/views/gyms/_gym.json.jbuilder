json.extract! gym, :id, :manager_id, :plan_manager_id, :cnpj, :name, :address, :cep, :city, :state, :phone, :cellphone, :lat, :long, :due_date, :created_at, :updated_at
json.url gym_url(gym, format: :json)
