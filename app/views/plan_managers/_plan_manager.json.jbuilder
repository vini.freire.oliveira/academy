json.extract! plan_manager, :id, :name, :price, :created_at, :updated_at
json.url plan_manager_url(plan_manager, format: :json)
