json.extract! plan_service_manager, :id, :plan_manager_id, :service_manager_id, :created_at, :updated_at
json.url plan_service_manager_url(plan_service_manager, format: :json)
