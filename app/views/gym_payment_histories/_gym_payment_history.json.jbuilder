json.extract! gym_payment_history, :id, :gym_id, :price, :payment_date, :created_at, :updated_at
json.url gym_payment_history_url(gym_payment_history, format: :json)
