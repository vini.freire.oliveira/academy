json.extract! equipament, :id, :gym_id, :name, :created_at, :updated_at
json.url equipament_url(equipament, format: :json)
