json.extract! exercise, :id, :gym_id, :name, :created_at, :updated_at
json.url exercise_url(exercise, format: :json)
