json.extract! service_client, :id, :name, :price, :created_at, :updated_at
json.url service_client_url(service_client, format: :json)
