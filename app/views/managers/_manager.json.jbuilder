json.extract! manager, :id, :user_id, :name, :phone, :created_at, :updated_at
json.url manager_url(manager, format: :json)
