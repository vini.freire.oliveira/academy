json.extract! service_manager, :id, :name, :price, :created_at, :updated_at
json.url service_manager_url(service_manager, format: :json)
