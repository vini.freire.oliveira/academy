json.extract! coach, :id, :user_id, :gym_id, :name, :address, :phone, :cpf, :created_at, :updated_at
json.url coach_url(coach, format: :json)
