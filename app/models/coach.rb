class Coach < ApplicationRecord
  belongs_to :user
  belongs_to :gym

  has_many :trainings
end
