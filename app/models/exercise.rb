class Exercise < ApplicationRecord
  belongs_to :gym

  has_many :trainings

end
