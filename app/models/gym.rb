class Gym < ApplicationRecord
  belongs_to :manager
  belongs_to :plan_manager

  has_many :coaches
  has_many :gym_payment_histories
  has_many :clients
  has_many :service_clients
  has_many :equipaments
  has_many :exercises
  has_many :numbers
  has_many :repetitions

end
