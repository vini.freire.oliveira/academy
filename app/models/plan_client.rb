class PlanClient < ApplicationRecord
  belongs_to :gym

  has_many :plan_service_clients
  has_many :clients

end
