class ClientMeasureHistory < ApplicationRecord
  belongs_to :client
  belongs_to :measure
end
