class Number < ApplicationRecord
  belongs_to :gym

  has_many :trainings

end
