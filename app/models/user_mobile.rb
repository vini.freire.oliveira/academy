class UserMobile < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          #:confirmable,
          :omniauthable
  include DeviseTokenAuth::Concerns::User

  def self.is_valid_token(token, client_id)
    @resource.valid_token?(token, client_id)
  end

end
