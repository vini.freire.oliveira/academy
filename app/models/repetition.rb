class Repetition < ApplicationRecord
  belongs_to :gym

  has_many :trainings

end
