class Training < ApplicationRecord
  belongs_to :coach
  belongs_to :equipament
  belongs_to :exercise
  belongs_to :number
  belongs_to :repetition
  belongs_to :muscle_type
end
