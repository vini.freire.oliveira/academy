class Client < ApplicationRecord
  belongs_to :user
  belongs_to :gym
  belongs_to :plan_client

  has_many :client_payment_histories
  has_many :training_clients
  has_many :client_measure_histories
end
