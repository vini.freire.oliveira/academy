class ManagersController < ApplicationController
  before_action :set_manager, only: [:show, :edit, :update, :destroy]
  before_action :set_user_manager, only: [:index, :show_clients, :show_coaches]
  before_action :manager_block_acess, except: [:index, :show_clients, :show_coaches]
  before_action :coach_block_acess
  before_action :authenticate_user!



  # GET /managers
  # GET /managers.json
  def index
  end

  # GET /managers/1
  # GET /managers/1.json
  def show
    @manager = Manager.find(params[:id])
  end

  # GET /managers/new
  def new
  end

  # GET /managers/1/edit
  def edit
  end

  def show_clients
  end

  def show_coaches
  end

  # POST /managers
  # POST /managers.json
  def create
    @user = User.new(user_params)
    if @user.save
      name = params[:manager][:name]
      phone = params[:manager][:phone]
      @manager = Manager.new(:user_id => @user.id, :name => name, :phone => phone)
      if @manager.save
        redirect_to gyms_path, notice: 'Gerente criado com sucesso'
      else
        redirect_to gyms_path, notice: 'Gerente não foi criado'
      end
    else
      redirect_to admins_path, notice: 'Usuário não foi criado'
    end
  end

  # PATCH/PUT /managers/1
  # PATCH/PUT /managers/1.json
  def update
    respond_to do |format|
      if @manager.update(manager_params)
        format.html { redirect_to @manager, notice: 'Manager was successfully updated.' }
        format.json { render :show, status: :ok, location: @manager }
      else
        format.html { render :edit }
        format.json { render json: @manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /managers/1
  # DELETE /managers/1.json
  def destroy
    @manager.destroy
    respond_to do |format|
      format.html { redirect_to managers_url, notice: 'Manager was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_manager
      @manager = Manager.find(params[:id])
    end

    def set_user_manager
      if current_user
        @manager = Manager.find_by_user_id(current_user)
        @gym = Gym.find_by_manager_id(@manager)
        @clients = Client.where(:gym_id => @gym ).all
        @coaches = Coach.where(:gym_id => @gym).all
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def manager_params
      params.require(:manager).permit(:user_id, :name, :phone)
    end

    def user_params
      params.require(:user).permit(:email,:password)
    end
end
