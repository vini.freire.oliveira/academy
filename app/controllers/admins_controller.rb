class AdminsController < ApplicationController
  before_action :set_admin, only: [:show, :edit, :update, :destroy]
  before_action :manager_block_acess
  before_action :authenticate_user!


  # GET /admins
  # GET /admins.json
  def index
    if current_user
      @admin = Admin.find_by_user_id(current_user)
      @managers = Manager.order('created_at DESC').all
      @gyms = Gym.order('created_at DESC').all
      @coaches = Coach.order('created_at DESC').all
      @clients = Client.order('created_at DESC').all
    end
  end

  # GET /admins/1
  # GET /admins/1.json
  def show
  end

  # GET /admins/new
  def new
    @admin = Admin.new
  end

  # GET /admins/1/edit
  def edit
  end

  def show_managers
    if current_user
      @admin = Admin.find_by_user_id(current_user)
      @managers = Manager.order('name DESC').all
    end
  end

  def show_manager
    if current_user
      @admin = Admin.find_by_user_id(current_user)
      if @admin
        @user = User.find_by_email(params[:manager][:email])
        @manager = Manager.find_by_user_id(@user)
      end
    end
  end


  # POST /admins
  # POST /admins.json
  def create
    @admin = Admin.new(admin_params)

    respond_to do |format|
      if @admin.save
        format.html { redirect_to @admin, notice: 'Admin was successfully created.' }
        format.json { render :show, status: :created, location: @admin }
      else
        format.html { render :new }
        format.json { render json: @admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admins/1
  # PATCH/PUT /admins/1.json
  def update
    respond_to do |format|
      if @admin.update(admin_params)
        format.html { redirect_to @admin, notice: 'Admin was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin }
      else
        format.html { render :edit }
        format.json { render json: @admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admins/1
  # DELETE /admins/1.json
  def destroy
    @admin.destroy
    respond_to do |format|
      format.html { redirect_to admins_url, notice: 'Admin was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin
      @admin = Admin.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_params
      params.require(:admin).permit(:user_id, :name)
    end
end
