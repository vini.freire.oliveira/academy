class PlanServiceClientsController < ApplicationController
  before_action :set_plan_service_client, only: [:show, :edit, :update, :destroy]
  before_action :coach_block_acess
  before_action :authenticate_user!

  # GET /plan_service_clients
  # GET /plan_service_clients.json
  def index
    @plan_service_clients = PlanServiceClient.all
  end

  # GET /plan_service_clients/1
  # GET /plan_service_clients/1.json
  def show
  end

  # GET /plan_service_clients/new
  def new
    @plan_service_client = PlanServiceClient.new
  end

  # GET /plan_service_clients/1/edit
  def edit
  end

  # POST /plan_service_clients
  # POST /plan_service_clients.json
  def create
    @plan_service_client = PlanServiceClient.new(plan_service_client_params)

    respond_to do |format|
      if @plan_service_client.save
        format.html { redirect_to @plan_service_client, notice: 'Plan service client was successfully created.' }
        format.json { render :show, status: :created, location: @plan_service_client }
      else
        format.html { render :new }
        format.json { render json: @plan_service_client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /plan_service_clients/1
  # PATCH/PUT /plan_service_clients/1.json
  def update
    respond_to do |format|
      if @plan_service_client.update(plan_service_client_params)
        format.html { redirect_to @plan_service_client, notice: 'Plan service client was successfully updated.' }
        format.json { render :show, status: :ok, location: @plan_service_client }
      else
        format.html { render :edit }
        format.json { render json: @plan_service_client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plan_service_clients/1
  # DELETE /plan_service_clients/1.json
  def destroy
    @plan_service_client.destroy
    respond_to do |format|
      format.html { redirect_to plan_service_clients_url, notice: 'Plan service client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_plan_service_client
      @plan_service_client = PlanServiceClient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def plan_service_client_params
      params.require(:plan_service_client).permit(:plan_client_id, :service_client_id)
    end
end
