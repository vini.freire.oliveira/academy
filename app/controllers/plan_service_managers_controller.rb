class PlanServiceManagersController < ApplicationController
  before_action :set_plan_service_manager, only: [:show, :edit, :update, :destroy]
  before_action :manager_block_acess
  before_action :authenticate_user!

  # GET /plan_service_managers
  # GET /plan_service_managers.json
  def index
    @plan_service_managers = PlanServiceManager.all
  end

  # GET /plan_service_managers/1
  # GET /plan_service_managers/1.json
  def show
  end

  # GET /plan_service_managers/new
  def new
    @plan_service_manager = PlanServiceManager.new
  end

  # GET /plan_service_managers/1/edit
  def edit
  end

  # POST /plan_service_managers
  # POST /plan_service_managers.json
  def create
    @plan_service_manager = PlanServiceManager.new(plan_service_manager_params)

    respond_to do |format|
      if @plan_service_manager.save
        format.html { redirect_to plan_service_managers_path, notice: 'Plan service manager was successfully created.' }
        format.json { render :show, status: :created, location: @plan_service_manager }
      else
        format.html { redirect_to plan_service_managers_path, notice: 'Plan service manager was not successfully created.' }
        format.json { render json: @plan_service_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /plan_service_managers/1
  # PATCH/PUT /plan_service_managers/1.json
  def update
    respond_to do |format|
      if @plan_service_manager.update(plan_service_manager_params)
        format.html { redirect_to @plan_service_manager, notice: 'Plan service manager was successfully updated.' }
        format.json { render :show, status: :ok, location: @plan_service_manager }
      else
        format.html { render :edit }
        format.json { render json: @plan_service_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plan_service_managers/1
  # DELETE /plan_service_managers/1.json
  def destroy
    @plan_service_manager.destroy
    respond_to do |format|
      format.html { redirect_to plan_service_managers_url, notice: 'Plan service manager was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_plan_service_manager
      @plan_service_manager = PlanServiceManager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def plan_service_manager_params
      params.require(:plan_service_manager).permit(:plan_manager_id, :service_manager_id)
    end
end
