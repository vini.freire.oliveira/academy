class ServiceClientsController < ApplicationController
  before_action :set_service_client, only: [:show, :edit, :update, :destroy]
  before_action :coach_block_acess
  before_action :authenticate_user!

  # GET /service_clients
  # GET /service_clients.json
  def index
    @manager = Manager.find_by_user_id(current_user)
    @gym = Gym.find_by_manager_id(@manager)
    @service_clients = ServiceClient.where(:gym_id => @gym.id).all
  end

  # GET /service_clients/1
  # GET /service_clients/1.json
  def show
  end

  # GET /service_clients/new
  def new
    @service_client = ServiceClient.new
  end

  # GET /service_clients/1/edit
  def edit
  end

  # POST /service_clients
  # POST /service_clients.json
  def create
    @manager = Manager.find_by_user_id(current_user)
    @gym = Gym.find_by_manager_id(@manager)
    @service_client = ServiceClient.new(service_client_params)
    @service_client.gym_id = @gym.id

    if @service_client.save
      redirect_to service_clients_path, notice: 'serviço criado com sucesso.'
    else
      redirect_to service_clients_path, notice: 'serviço criado com sucesso.'
    end
  end

  # PATCH/PUT /service_clients/1
  # PATCH/PUT /service_clients/1.json
  def update
    respond_to do |format|
      if @service_client.update(service_client_params)
        format.html { redirect_to @service_client, notice: 'Service client was successfully updated.' }
        format.json { render :show, status: :ok, location: @service_client }
      else
        format.html { render :edit }
        format.json { render json: @service_client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /service_clients/1
  # DELETE /service_clients/1.json
  def destroy
    @service_client.destroy
    respond_to do |format|
      format.html { redirect_to service_clients_url, notice: 'Service client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_client
      @service_client = ServiceClient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_client_params
      params.require(:service_client).permit(:name, :price)
    end
end
