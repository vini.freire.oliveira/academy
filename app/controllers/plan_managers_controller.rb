class PlanManagersController < ApplicationController
  before_action :set_plan_manager, only: [:show, :edit, :update, :destroy]
  before_action :manager_block_acess
  before_action :authenticate_user!

  # GET /plan_managers
  # GET /plan_managers.json
  def index
    if current_user
      @plan_managers = PlanManager.all
      @admin = Admin.find_by_user_id(current_user)
    end
  end

  # GET /plan_managers/1
  # GET /plan_managers/1.json
  def show
    @services = PlanServiceManager.where(:plan_manager_id => @plan_manager.id).all
    name = Array.new
    price = Array.new
    @services.each do |s|
      name.push(s.service_manager.name)
      price.push(s.service_manager.price)
    end
    @response = {"service": name,"price": price}
    render_json(200,@response)
  end

  # GET /plan_managers/new
  def new
    @plan_manager = PlanManager.new
    @service_managers = ServiceManager.all.order('name ASC')
  end

  # GET /plan_managers/1/edit
  def edit
  end

  # POST /plan_managers
  # POST /plan_managers.json
  def create
    @plan_manager = PlanManager.new(plan_manager_params)
    @plan_manager.price = 0
    if @plan_manager.save
      @service_idies = params[:plan_service_manager][:service_manager_id]
      @service_idies.each do |servico_id|
        @service_manager = ServiceManager.find(servico_id)
        PlanServiceManager.create(:plan_manager_id => @plan_manager.id, :service_manager_id => servico_id)
        @plan_manager.price += @service_manager.price
      end

      if @plan_manager.update(:name => @plan_manager.name, :price => @plan_manager.price)
        redirect_to plan_managers_path, notice: 'Plano cadastrado com sucesso'
      else
        redirect_to plan_managers_path, notice: 'Impossivel calcular o valor do plano'
      end

    else
      redirect_to plan_managers_path, notice: 'Plano não foi cadastrado!'
    end

  end

  # PATCH/PUT /plan_managers/1
  # PATCH/PUT /plan_managers/1.json
  def update
    respond_to do |format|
      if @plan_manager.update(plan_manager_params)
        format.html { redirect_to @plan_manager, notice: 'Plan manager was successfully updated.' }
        format.json { render :show, status: :ok, location: @plan_manager }
      else
        format.html { render :edit }
        format.json { render json: @plan_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plan_managers/1
  # DELETE /plan_managers/1.json
  def destroy

    @service_managers = PlanServiceManager.where(:plan_manager_id => @plan_manager.id).all
    @service_managers.each do |s|
      s.destroy
    end

    @plan_manager.destroy
    respond_to do |format|
      format.html { redirect_to plan_managers_url, notice: 'Plan manager was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_plan_manager
      @plan_manager = PlanManager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def plan_manager_params
      params.require(:plan_manager).permit(:name, :price)
    end
end
