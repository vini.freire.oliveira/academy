class RepetitionsController < ApplicationController
  before_action :set_repetition, only: [:show, :edit, :update, :destroy]
  before_action :client_block_acess, except: [:index]
  before_action :authenticate_user!

  # GET /repetitions
  # GET /repetitions.json
  def index
    if current_user
      @admin = Admin.find_by_user_id(current_user)
      @manager = Manager.find_by_user_id(current_user)
      @coach = Coach.find_by_user_id(current_user)
      @client = Client.find_by_user_id(current_user)

      if @admin
        @repetitions = Repetition.all
      elsif @manager
        @gym = Gym.find_by_manager_id(@manager)
        @repetitions = Repetition.where(:gym_id => @gym.id).order('created_at ASC').all
      elsif @coach
        @repetitions = Repetition.where(:gym_id => @coach.gym_id).order('created_at ASC').all
      elsif @client
        @repetitions = Repetition.where(:gym_id => @client.gym_id).order('created_at ASC').all
      end
    end
  end

  # GET /repetitions/1
  # GET /repetitions/1.json
  def show
  end

  # GET /repetitions/new
  def new
    @repetition = Repetition.new
  end

  # GET /repetitions/1/edit
  def edit
  end

  # POST /repetitions
  # POST /repetitions.json
  def create
    if current_user
      @manager = Manager.find_by_user_id(current_user)
      @coach = Coach.find_by_user_id(current_user)
      @number = params[:repetition][:number]

      if @manager
        @gym = Gym.find_by_manager_id(@manager)
        @repetition = Repetition.new(:gym_id => @gym.id, :number => @number)
        if @repetition.save
          redirect_to repetitions_path, notice: 'Cadastrada com sucesso'
        else
          redirect_to repetitions_path, notice: 'Não cadastrado'
        end
      elsif @coach
        @repetition = Repetition.new(:gym_id => @coach.gym_id, :number => @number)
        if @repetition.save
          redirect_to repetitions_path, notice: 'Cadastrado com sucesso'
        else
          redirect_to repetitions_path, notice: 'Não cadastrado'
        end
      end
    end
  end

  # PATCH/PUT /repetitions/1
  # PATCH/PUT /repetitions/1.json
  def update
    respond_to do |format|
      if @repetition.update(repetition_params)
        format.html { redirect_to @repetition, notice: 'Repetition was successfully updated.' }
        format.json { render :show, status: :ok, location: @repetition }
      else
        format.html { render :edit }
        format.json { render json: @repetition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /repetitions/1
  # DELETE /repetitions/1.json
  def destroy
    @repetition.destroy
    respond_to do |format|
      format.html { redirect_to repetitions_url, notice: 'Repetition was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_repetition
      @repetition = Repetition.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def repetition_params
      params.require(:repetition).permit(:gym_id, :number)
    end
end
