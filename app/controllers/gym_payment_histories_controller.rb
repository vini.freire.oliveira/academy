class GymPaymentHistoriesController < ApplicationController
  before_action :set_gym_payment_history, only: [:show, :edit, :update, :destroy]
  before_action :manager_block_acess, except: [:index, :show]
  before_action :authenticate_user!

  # GET /gym_payment_histories
  # GET /gym_payment_histories.json
  def index
    if current_user
      @admin = Admin.find_by_user_id(current_user)
      @manager = Manager.find_by_user_id(current_user)

      if @admin
        @gym_payment_histories = GymPaymentHistory.order("payment_date DESC").all
      elsif @manager
        @gym = Gym.find_by_manager_id(@manager)
        @gym_payment_histories = GymPaymentHistory.where(:gym_id => @gym ).order("payment_date DESC").all
      end

    end

  end

  # GET /gym_payment_histories/1
  # GET /gym_payment_histories/1.json
  def show
  end

  # GET /gym_payment_histories/new
  def new
    @gym_payment_history = GymPaymentHistory.new
    @gyms = Gym.all
  end

  # GET /gym_payment_histories/1/edit
  def edit
  end

  # POST /gym_payment_histories
  # POST /gym_payment_histories.json
  def create
    @gym_payment_history = GymPaymentHistory.new(gym_payment_history_params)

    if @gym_payment_history.save
      redirect_to gym_payment_histories_path, notice: 'Pagamento efetuado com sucesso'
    else
      redirect_to gym_payment_histories_path, notice: 'Pagamento efetuado com sucesso'
    end
  end

  # PATCH/PUT /gym_payment_histories/1
  # PATCH/PUT /gym_payment_histories/1.json
  def update
    respond_to do |format|
      if @gym_payment_history.update(gym_payment_history_params)
        format.html { redirect_to @gym_payment_history, notice: 'Gym payment history was successfully updated.' }
        format.json { render :show, status: :ok, location: @gym_payment_history }
      else
        format.html { render :edit }
        format.json { render json: @gym_payment_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gym_payment_histories/1
  # DELETE /gym_payment_histories/1.json
  def destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gym_payment_history
      @gym_payment_history = GymPaymentHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gym_payment_history_params
      params.require(:gym_payment_history).permit(:gym_id, :price, :payment_date)
    end
end
