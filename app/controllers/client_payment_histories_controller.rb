class ClientPaymentHistoriesController < ApplicationController
  before_action :set_client_payment_history, only: [:show, :edit, :update, :destroy]
  before_action :coach_block_acess,  except: [:index]
  before_action :authenticate_user!

  # GET /client_payment_histories
  # GET /client_payment_histories.json
  def index
    if current_user
      @admin = Admin.find_by_user_id(current_user)
      @manager = Manager.find_by_user_id(current_user)
      @client = Client.find_by_user_id(current_user)

      if @admin
        @client_payment_histories = ClientPaymentHistory.order("payment_day DESC").all
      elsif @manager
        @gym = Gym.find_by_manager_id(@manager)
        @clients = Client.where(:gym_id => @gym ).all
        @client_payment_histories = ClientPaymentHistory.where(:client_id => @clients).order("payment_day DESC").all
      elsif @client
        @client_payment_histories = ClientPaymentHistory.where(:client_id => @client).order("payment_day DESC").all
      end

    end

  end

  # GET /client_payment_histories/1
  # GET /client_payment_histories/1.json
  def show
  end

  # GET /client_payment_histories/new
  def new
    @client_payment_history = ClientPaymentHistory.new

    if current_user
      @manager = Manager.find_by_user_id(current_user)

      if @manager
        @gym = Gym.find_by_manager_id(@manager)
        @clients = Client.where(:gym_id => @gym ).all
      end

    end
  end

  # GET /client_payment_histories/1/edit
  def edit
  end

  # POST /client_payment_histories
  # POST /client_payment_histories.json
  def create
    @client_payment_history = ClientPaymentHistory.new(client_payment_history_params)

    if @client_payment_history.save
      redirect_to client_payment_histories_path, notice: 'Pagamento efetuado com sucesso'
    else
      redirect_to client_payment_histories_path, notice: 'Pagamento não efetuado'
    end
  end

  # PATCH/PUT /client_payment_histories/1
  # PATCH/PUT /client_payment_histories/1.json
  def update
    respond_to do |format|
      if @client_payment_history.update(client_payment_history_params)
        format.html { redirect_to @client_payment_history, notice: 'Client payment history was successfully updated.' }
        format.json { render :show, status: :ok, location: @client_payment_history }
      else
        format.html { render :edit }
        format.json { render json: @client_payment_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /client_payment_histories/1
  # DELETE /client_payment_histories/1.json
  def destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client_payment_history
      @client_payment_history = ClientPaymentHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_payment_history_params
      params.require(:client_payment_history).permit(:client_id, :price, :payment_day)
    end
end
