class PlanClientsController < ApplicationController
  before_action :set_plan_client, only: [:show, :edit, :update, :destroy]
  before_action :coach_block_acess
  before_action :authenticate_user!

  # GET /plan_clients
  # GET /plan_clients.json
  def index
    @manager = Manager.find_by_user_id(current_user)
    @gym = Gym.find_by_manager_id(@manager)
    @plan_clients = PlanClient.where(:gym_id => @gym.id).all
  end

  # GET /plan_clients/1
  # GET /plan_clients/1.json
  def show
    @services = PlanServiceClient.where(:plan_client_id => @plan_client.id).all
    name = Array.new
    price = Array.new
    @services.each do |s|
      name.push(s.service_client.name)
      price.push(s.service_client.price)
    end
    @response = {"service": name,"price": price}
    render_json(200,@response)
  end

  # GET /plan_clients/new
  def new
    @plan_client = PlanClient.new
    @manager = Manager.find_by_user_id(current_user)
    @gym = Gym.find_by_manager_id(@manager)
    @service_clients = ServiceClient.where(:gym_id => @gym.id).all.order('name ASC')
  end

  # GET /plan_clients/1/edit
  def edit
  end


  # POST /plan_clients
  # POST /plan_clients.json
  def create
    @plan_client = PlanClient.new(plan_client_params)
    @plan_client.price = 0
    @manager = Manager.find_by_user_id(current_user)
    @gym = Gym.find_by_manager_id(@manager)
    @plan_client.gym_id = @gym.id

    if @plan_client.save
      @service_idies = params[:plan_service_client][:service_client_id]
      @service_idies.each do |servico_id|
        @service_client = ServiceClient.find(servico_id)
        PlanServiceClient.create(:plan_client_id => @plan_client.id, :service_client_id => servico_id)
        @plan_client.price += @service_client.price
      end

      if @plan_client.update(:name => @plan_client.name, :price => @plan_client.price)
        redirect_to plan_clients_path, notice: 'Plano cadastrado com sucesso'
      else
        redirect_to plan_clients_path, notice: 'Impossivel calcular o valor do plano'
      end

    else
      redirect_to plan_clients_path, notice: 'Plano não foi cadastrado!'
    end

  end

  # PATCH/PUT /plan_clients/1
  # PATCH/PUT /plan_clients/1.json
  def update
    respond_to do |format|
      if @plan_client.update(plan_client_params)
        format.html { redirect_to @plan_client, notice: 'Plan client was successfully updated.' }
        format.json { render :show, status: :ok, location: @plan_client }
      else
        format.html { render :edit }
        format.json { render json: @plan_client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plan_clients/1
  # DELETE /plan_clients/1.json
  def destroy

    @plan_service_clients = PlanServiceClient.where(:plan_client_id => @plan_client.id).all
    @plan_service_clients.each do |s|
      s.destroy
    end

    @plan_client.destroy
    respond_to do |format|
      format.html { redirect_to plan_clients_url, notice: 'Plan client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_plan_client
      @plan_client = PlanClient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def plan_client_params
      params.require(:plan_client).permit(:gym_id, :name, :price)
    end
end
