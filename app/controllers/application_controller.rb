class ApplicationController < ActionController::Base
  include ApplicationHelper
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!, only: [:admin_page, :manager_page, :coach_page, :client_page, :secret]

  def home
    if current_user
      @admin = Admin.find_by_user_id(current_user)
      @manager = Manager.find_by_user_id(current_user)
      @coach = Coach.find_by_user_id(current_user)
      @client = Client.find_by_user_id(current_user)
      if @admin
        admin_page
      elsif @manager
        manager_page
      elsif @coach
        coach_page
      elsif @client
        client_page
      end
    end
  end

  def admin_page
    redirect_to admins_path, notice: 'Administrador logado'
  end

  def manager_page
    redirect_to managers_path, notice: @manager.name + 'Gerente Logado'
  end

  def coach_page
    redirect_to coaches_path, notice: @coach.name + 'Profissional Logado'
  end

  def client_page
    redirect_to clients_path, notice: @client.name + 'Aluno Logado'
  end

  def secret
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name])
  end


end
