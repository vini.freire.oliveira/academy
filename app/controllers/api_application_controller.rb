class ApiApplicationController < ActionController::Base
  DeviseTokenAuth::Concerns::SetUserByToken
  include ApiApplicationHelper
  include Devise::Controllers::Helpers

  def resource_params
      params.permit(devise_parameter_sanitizer.for(:sign_in))
  end

end
