class ClientMeasureHistoriesController < ApplicationController
  before_action :set_client_measure_history, only: [:show, :edit, :update, :destroy]
  before_action :client_block_acess, except: [:index,:show]
  before_action :authenticate_user!


  # GET /client_measure_histories
  # GET /client_measure_histories.json
  def index
    if current_user
      @admin = Admin.find_by_user_id(current_user)
      @manager = Manager.find_by_user_id(current_user)
      @coach = Coach.find_by_user_id(current_user)
      @client = Client.find_by_user_id(current_user)
      if @admin
        @client_measure_histories = ClientMeasureHistory.order('data_measures DESC').all
      elsif @manager
        @gym = Gym.find_by_manager_id(@manager)
        @clients = Client.where(:gym_id => @gym ).all
        @client_measure_histories = ClientMeasureHistory.where(:client_id => @clients).order('data_measures DESC').all
      elsif @coach
        @clients = Client.where(:gym_id => @coach.gym_id ).all
        @client_measure_histories = ClientMeasureHistory.where(:client_id => @clients).order('data_measures DESC').all
      elsif @client
        @client_measure_histories = ClientMeasureHistory.where(:client_id => @client).order('created_at DESC').all
      end
    end
  end

  # GET /client_measure_histories/1
  # GET /client_measure_histories/1.json
  def show
  end

  # GET /client_measure_histories/new
  def new
    if current_user
      @coach = Coach.find_by_user_id(current_user)
      if @coach
        @clients = Client.where(:gym_id => @coach.gym_id).all.order('name ASC')
      end
    end
  end

  # GET /client_measure_histories/1/edit
  def edit
  end

  # POST /client_measure_histories
  # POST /client_measure_histories.json
  def create
    @measure = Measure.new(measure_params)
    if @measure.save
      @client_id = params[:client_measure_history][:client_id]
      @data_measures = Time.now
      @client_measure_history = ClientMeasureHistory.new(:client_id => @client_id, :measure_id => @measure.id, :data_measures => @data_measures)
      if @client_measure_history.save
        redirect_to client_measure_histories_path, notice: 'medidas salvas'
      else
        redirect_to client_measure_histories_path, notice: 'medidas não foram alocadas para o cliente'
      end
    else
      redirect_to client_measure_histories_path, notice: 'medidas não foram salvas'
    end
  end

  # PATCH/PUT /client_measure_histories/1
  # PATCH/PUT /client_measure_histories/1.json
  def update
    respond_to do |format|
      if @client_measure_history.update(client_measure_history_params)
        format.html { redirect_to @client_measure_history, notice: 'Client measure history was successfully updated.' }
        format.json { render :show, status: :ok, location: @client_measure_history }
      else
        format.html { render :edit }
        format.json { render json: @client_measure_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /client_measure_histories/1
  # DELETE /client_measure_histories/1.json
  def destroy
    @client_measure_history.destroy
    respond_to do |format|
      format.html { redirect_to client_measure_histories_url, notice: 'Client measure history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client_measure_history
      @client_measure_history = ClientMeasureHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_measure_history_params
      params.require(:client_measure_history).permit(:client_id, :measures_id, :data_measures)
    end

    def measure_params
      params.require(:measure).permit(:weight, :height, :waist, :pectoral, :hip, :arm_right, :arm_left, :thigh_right, :thigh_left, :calf_right, :calf_left)
    end
end
