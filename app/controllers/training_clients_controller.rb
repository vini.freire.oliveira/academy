class TrainingClientsController < ApplicationController
  before_action :set_training_client, only: [:show, :edit, :update, :destroy]
  before_action :client_block_acess, except: [:index,:show]
  before_action :authenticate_user!

  # GET /training_clients
  # GET /training_clients.json
  def index
    if current_user
      @admin = Admin.find_by_user_id(current_user)
      @manager = Manager.find_by_user_id(current_user)
      @coach = Coach.find_by_user_id(current_user)
      @client = Client.find_by_user_id(current_user)
      if @admin
        @training_clients = TrainingClient.all
      elsif @manager
        @gym = Gym.find_by_manager_id(@manager)
        @clients = Client.where(:gym_id => @gym ).all
        @training_clients = TrainingClient.where(:client_id => @clients).all
      elsif @coach
        @trainings = Training.where(:coach_id => @coach).all
        @training_clients = TrainingClient.where(:training_id => @trainings).order('client_id ASC').order(:group).all
      elsif @client
        @training_clients = TrainingClient.where(:client_id => @client).all
      end
    end
  end

  # GET /training_clients/1
  # GET /training_clients/1.json
  def show
  end

  # GET /training_clients/new
  def new
    if current_user
      @coach = Coach.find_by_user_id(current_user)
      if @coach
        @clients = Client.where(:gym_id => @coach.gym_id).all.order('name ASC')
        @equipaments = Equipament.where(:gym_id => @coach.gym_id).all.order('name ASC')
        @exercises = Exercise.where(:gym_id => @coach.gym_id).all.order('name ASC')
        @numbers = Number.where(:gym_id => @coach.gym_id).all.order('number ASC')
        @repetitions = Repetition.where(:gym_id => @coach.gym_id).all.order('number ASC')
        @muscle_types = MuscleType.all
      end
    end
  end

  # GET /training_clients/1/edit
  def edit
    if current_user
      @coach = Coach.find_by_user_id(current_user)
      if @coach
        @clients = Client.where(:id => @training_client.client_id)
        @equipaments = Equipament.where(:gym_id => @coach.gym_id).all.order('name ASC')
        @exercises = Exercise.where(:gym_id => @coach.gym_id).all.order('name ASC')
        @numbers = Number.where(:gym_id => @coach.gym_id).all.order('number ASC')
        @repetitions = Repetition.where(:gym_id => @coach.gym_id).all.order('number ASC')
        @muscle_types = MuscleType.all
      end
    end
  end

  # POST /training_clients
  # POST /training_clients.json
  def create
    if current_user
      @coach = Coach.find_by_user_id(current_user)
      if @coach
        @equipaments_id = params[:training][:equipaments_id]
        @exercise_id = params[:training][:exercise_id]
        @muscle_type_id = params[:training][:muscle_type_id]
        @number_id = params[:training][:number_id]
        @repetition_id = params[:training][:repetition_id]
        @how_make = params[:training][:how_make]
        @training = Training.new(:coach_id => @coach.id, :equipament_id => @equipaments_id, :exercise_id => @exercise_id, :number_id => @number_id, :repetition_id => @repetition_id, :muscle_type_id => @muscle_type_id, :how_make => @how_make )
        if @training.save
          @client_id = params[:training_client][:client_id]
          @group = params[:training_client][:group]
          @turn = params[:training_client][:turn]
          @training_client = TrainingClient.new(:client_id => @client_id, :training_id => @training.id, :group => @group, :turn =>@turn)
          if @training_client.save
            redirect_to training_clients_path, notice: 'Treinamento criado e salvo para o cliente'
          else
            redirect_to training_clients_path, notice: 'Treinamento craiado e não salvo para o cliente'
          end
        else
          redirect_to training_clients_path, notice: 'Treinamento não foi criado'
        end
      end
    end
  end

  # PATCH/PUT /training_clients/1
  # PATCH/PUT /training_clients/1.json
  def update
    if current_user
      @coach = Coach.find_by_user_id(current_user)
      if @coach
        @equipament_id = params[:training][:equipaments_id]
        @exercise_id = params[:training][:exercise_id]
        @muscle_type_id = params[:training][:muscle_type_id]
        @number_id = params[:training][:number_id]
        @repetition_id = params[:training][:repetition_id]
        @how_make = params[:training][:how_make]
        @training = Training.find(training_client.training_id)
        @training.equipament_id = @equipament_id
        @training.exercise_id = @exercise_id
        @training.muscle_type_id = @muscle_type_id
        @training.number_id = @number_id
        @training.repetition_id = @repetition_id
        @training.how_make = @how_make
        if @training.update
          @group = params[:training_client][:group]
          @turn = params[:training_client][:turn]
          @training_client.group = @group
          @training_client.turn = @turn
          if @training_client.update
            redirect_to training_clients_path, notice: 'Treinamento atualizado com sucesso'
          else
            redirect_to training_clients_path, notice: 'Falha ao alocar o treinamento'
          end
        else
          redirect_to training_clients_path, notice: 'Treinamento não foi atualizado'
        end
      end
    end
  end

  # DELETE /training_clients/1
  # DELETE /training_clients/1.json
  def destroy
    @training_client.destroy
    respond_to do |format|
      format.html { redirect_to training_clients_url, notice: 'Training client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_training_client
      @training_client = TrainingClient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def training_client_params
      params.require(:training_client).permit(:client_id, :training_id, :group, :turn)
    end
end
