class CoachesController < ApplicationController
  before_action :set_coach, only: [:edit, :update, :destroy]
  before_action :coach_block_acess, except: [:index]
  before_action :authenticate_user!


  # GET /coaches
  # GET /coaches.json
  def index
    if current_user
      @coach = Coach.find_by_user_id(current_user)
      if @coach
        @trainings = Training.where(:coach_id => @coach.id).all
        @training_clients = TrainingClient.where(:training_id => @trainings).all
        @my_clients = Client.where(:id => @training_clients).all
      end
    end
  end

  # GET /coaches/1
  # GET /coaches/1.json
  def show
    @user = User.find_by_email(params[:coach][:email])
    @coach = Coach.find_by_user_id(@user)
    if @coach
      @trainings = Training.where(:coach_id => @coach.id).all
      @client_trainings = TrainingClient.where(:training_id => @trainings).order('client_id ASC').all
    else
      redirect_to "/manager_coaches", alert: 'Profissional não encontrado!'
    end
  end

  # GET /coaches/new
  def new
    @coach = Coach.new

    if current_user
      @admin = Admin.find_by_user_id(current_user)
      @manager = Manager.find_by_user_id(current_user)

      if @admin
        @gyms = Gym.order('name ASC').all
      elsif @manager
        @gyms = Gym.order('name ASC').where(:manager_id => @manager).all
      end
    end
  end

  # GET /coaches/1/edit
  def edit
  end

  # POST /coaches
  # POST /coaches.json
  def create
    @user = User.new(user_params)

    if @user.save
      gym_id = params[:coach][:gym_id]
      address = params[:coach][:address]
      phone = params[:coach][:phone]
      name = params[:coach][:name]
      cpf = params[:coach][:cpf]

      @coach = Coach.new(:user_id => @user.id,:gym_id => gym_id, :address => address, :phone => phone, :name => name, :cpf => cpf)
      if @coach.save
        redirect_to home_path, notice: 'Profissional criado com sucesso'
      else
        redirect_to home_path, notice: 'Profissional não foi criado'
      end
    else
      redirect_to home_path, notice: 'Usuário não foi criado'
    end

  end

  # PATCH/PUT /coaches/1
  # PATCH/PUT /coaches/1.json
  def update
    respond_to do |format|
      if @coach.update(coach_params)
        format.html { redirect_to @coach, notice: 'Coach was successfully updated.' }
        format.json { render :show, status: :ok, location: @coach }
      else
        format.html { render :edit }
        format.json { render json: @coach.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coaches/1
  # DELETE /coaches/1.json
  def destroy
    @coach.destroy
    respond_to do |format|
      format.html { redirect_to coaches_url, notice: 'Coach was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coach
      @coach = Coach.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def coach_params
      params.require(:coach).permit(:user_id, :gym_id, :name, :address, :phone, :cpf)
    end

    def user_params
      params.require(:user).permit(:email,:password)
    end
end
