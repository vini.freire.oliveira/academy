class ExercisesController < ApplicationController
  before_action :set_exercise, only: [:show, :edit, :update, :destroy]
  before_action :client_block_acess, except: [:index]
  before_action :authenticate_user!

  # GET /exercises
  # GET /exercises.json
  def index
    if current_user
      @admin = Admin.find_by_user_id(current_user)
      @manager = Manager.find_by_user_id(current_user)
      @coach = Coach.find_by_user_id(current_user)
      @client = Client.find_by_user_id(current_user)

      if @admin
        @exercises = Exercise.all
      elsif @manager
        @gym = Gym.find_by_manager_id(@manager)
        @exercises = Exercise.where(:gym_id => @gym.id).order('name ASC').all
      elsif @coach
        @exercises = Exercise.where(:gym_id => @coach.gym_id).order('name ASC').all
      elsif @client
        @exercises = Exercise.where(:gym_id => @client.gym_id).order('name ASC').all
      end
    end
  end

  # GET /exercises/1
  # GET /exercises/1.json
  def show
  end

  # GET /exercises/new
  def new
    @exercise = Exercise.new
  end

  # GET /exercises/1/edit
  def edit
  end

  # POST /exercises
  # POST /exercises.json
  def create
    if current_user
      @manager = Manager.find_by_user_id(current_user)
      @coach = Coach.find_by_user_id(current_user)
      @name = params[:exercise][:name]

      if @manager
        @gym = Gym.find_by_manager_id(@manager)
        @exercise = Exercise.new(:gym_id => @gym.id, :name => @name)
        if @exercise.save
          redirect_to exercises_path, notice: 'Exercício cadastrado com sucesso'
        else
          redirect_to exercises_path, notice: 'Exercício não cadastrado'
        end
      elsif @coach
        @exercise = Exercise.new(:gym_id => @coach.gym_id, :name => @name)
        if @exercise.save
          redirect_to exercises_path, notice: 'Exercício cadastrado com sucesso'
        else
          redirect_to exercises_path, notice: 'Exercício não cadastrado'
        end
      end
    end
  end

  # PATCH/PUT /exercises/1
  # PATCH/PUT /exercises/1.json
  def update
    respond_to do |format|
      if @exercise.update(exercise_params)
        format.html { redirect_to @exercise, notice: 'Exercise was successfully updated.' }
        format.json { render :show, status: :ok, location: @exercise }
      else
        format.html { render :edit }
        format.json { render json: @exercise.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /exercises/1
  # DELETE /exercises/1.json
  def destroy
    @exercise.destroy
    respond_to do |format|
      format.html { redirect_to exercises_url, notice: 'Exercise was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exercise
      @exercise = Exercise.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def exercise_params
      params.require(:exercise).permit(:gym_id, :name)
    end
end
