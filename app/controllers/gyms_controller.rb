class GymsController < ApplicationController
  before_action :set_gym, only: [:show, :edit, :update, :destroy, :payments]
  before_action :manager_block_acess
  before_action :authenticate_user!

  # GET /gyms
  # GET /gyms.json
  def index
    @gyms = Gym.all
    if current_user
      @admin = Admin.find_by_user_id(current_user)
    end
  end

  # GET /gyms/1
  # GET /gyms/1.json
  def show
  end

  # GET /gyms/new
  def new
    @managers = Manager.order('name ASC').all
    @plan_managers = PlanManager.order('price DESC').all
  end

  # GET /gyms/1/edit
  def edit
  end

  def payments
    if current_user
      @payments = GymPaymentHistory.where(:gym_id => @gym).order('payment_date DESC').all
      render_json(200,@payments)
    end
  end

  # POST /gyms
  # POST /gyms.json
  def create
    @gym = Gym.new(gym_params)

    if @gym.save
      redirect_to gyms_path, notice: 'Academia criado com sucesso'
    else
      redirect_to gyms_path, notice: 'Academia não foi cadastrada!'
    end

  end

  # PATCH/PUT /gyms/1
  # PATCH/PUT /gyms/1.json
  def update
    respond_to do |format|
      if @gym.update(gym_params)
        format.html { redirect_to @gym, notice: 'Gym was successfully updated.' }
        format.json { render :show, status: :ok, location: @gym }
      else
        format.html { render :edit }
        format.json { render json: @gym.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gyms/1
  # DELETE /gyms/1.json
  def destroy
    @gym.destroy
    respond_to do |format|
      format.html { redirect_to gyms_url, notice: 'Gym was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gym
      @gym = Gym.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gym_params
      params.require(:gym).permit(:manager_id, :plan_manager_id, :cnpj, :name, :address, :cep, :city, :state, :phone, :cellphone, :lat, :lon, :due_date)
    end
end
