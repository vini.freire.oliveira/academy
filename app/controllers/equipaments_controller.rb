class EquipamentsController < ApplicationController
  before_action :set_equipament, only: [:show, :edit, :update, :destroy]
  before_action :client_block_acess, except: [:index]
  before_action :authenticate_user!

  # GET /equipaments
  # GET /equipaments.json
  def index
    if current_user
      @admin = Admin.find_by_user_id(current_user)
      @manager = Manager.find_by_user_id(current_user)
      @coach = Coach.find_by_user_id(current_user)
      @client = Client.find_by_user_id(current_user)

      if @admin
        @equipaments = Equipament.all
      elsif @manager
        @gym = Gym.find_by_manager_id(@manager)
        @equipaments = Equipament.where(:gym_id => @gym.id).order('name ASC').all
      elsif @coach
        @equipaments = Equipament.where(:gym_id => @coach.gym_id).order('name ASC').all
      elsif @client
        @equipaments = Equipament.where(:gym_id => @client.gym_id).order('name ASC').all
      end

    end

  end

  # GET /equipaments/1
  # GET /equipaments/1.json
  def show
  end

  # GET /equipaments/new
  def new
    @equipament = Equipament.new
  end

  # GET /equipaments/1/edit
  def edit
  end

  # POST /equipaments
  # POST /equipaments.json
  def create
    if current_user
      @manager = Manager.find_by_user_id(current_user)
      @coach = Coach.find_by_user_id(current_user)
      @name = params[:equipament][:name]

      if @manager
        @gym = Gym.find_by_manager_id(@manager)
        @equipament = Equipament.new(:gym_id => @gym.id, :name => @name)
        if @equipament.save
          redirect_to equipaments_path, notice: 'Equipamento cadastrado com sucesso'
        else
          redirect_to equipaments_path, notice: 'Equipamento não cadastrado'
        end
      elsif @coach
        @equipament = Equipament.new(:gym_id => @coach.gym_id, :name => @name)
        if @equipament.save
          redirect_to equipaments_path, notice: 'Equipamento cadastrado com sucesso'
        else
          redirect_to equipaments_path, notice: 'Equipamento não cadastrado'
        end
      end

    end
  end

  # PATCH/PUT /equipaments/1
  # PATCH/PUT /equipaments/1.json
  def update
    respond_to do |format|
      if @equipament.update(equipament_params)
        format.html { redirect_to @equipament, notice: 'Equipament was successfully updated.' }
        format.json { render :show, status: :ok, location: @equipament }
      else
        format.html { render :edit }
        format.json { render json: @equipament.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /equipaments/1
  # DELETE /equipaments/1.json
  def destroy
    @equipament.destroy
    respond_to do |format|
      format.html { redirect_to equipaments_url, notice: 'Equipament was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_equipament
      @equipament = Equipament.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def equipament_params
      params.require(:equipament).permit(:gym_id, :name)
    end
end
