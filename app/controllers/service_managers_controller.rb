class ServiceManagersController < ApplicationController
  before_action :set_service_manager, only: [:show, :edit, :update, :destroy]
  before_action :manager_block_acess
  before_action :authenticate_user!

  # GET /service_managers
  # GET /service_managers.json
  def index
    @service_managers = ServiceManager.all
    if current_user
      @admin = Admin.find_by_user_id(current_user)
    end
  end

  # GET /service_managers/1
  # GET /service_managers/1.json
  def show
  end

  # GET /service_managers/new
  def new
    @service_manager = ServiceManager.new
  end

  # GET /service_managers/1/edit
  def edit
  end

  # POST /service_managers
  # POST /service_managers.json
  def create
    @service_manager = ServiceManager.new(service_manager_params)
    if @service_manager.save
      redirect_to service_managers_path, notice: 'cadastrado'
    else
      redirect_to service_managers_path, notice: 'Não cadastrado'
    end
  end

  # PATCH/PUT /service_managers/1
  # PATCH/PUT /service_managers/1.json
  def update
    respond_to do |format|
      if @service_manager.update(service_manager_params)
        format.html { redirect_to @service_manager, notice: 'Service manager was successfully updated.' }
        format.json { render :show, status: :ok, location: @service_manager }
      else
        format.html { render :edit }
        format.json { render json: @service_manager.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /service_managers/1
  # DELETE /service_managers/1.json
  def destroy
    @service_manager.destroy
    respond_to do |format|
      format.html { redirect_to service_managers_url, notice: 'Service manager was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_manager
      @service_manager = ServiceManager.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_manager_params
      params.require(:service_manager).permit(:name, :price)
    end
end
