class MobileController < ApiApplicationController
  DeviseTokenAuth::Concerns::SetUserByToken
  before_action :authenticate_user_mobile

  def mobile_profile
    if @user_mobile.valid_token?(@token, @client_id)
      @user = User.find_by_email(@user_mobile.email)
      @client = Client.find_by_user_id(@user)
      @coach = Coach.find_by_user_id(@user)
      if @client
        @gym = Gym.find(@client.gym_id)
        @plan_client = PlanClient.find(@client.plan_client_id)
        @response = {"profile": @client,"gym": @gym, "plan_client": @plan_client}
        render_json(200,@response)
      elsif @coach
        render_json(200,@coach)
      end
    else
      render_json(401,"Usuário não autorizado!")
    end
  end


  def profile_payments
  if @user_mobile.valid_token?(@token, @client_id)
    @user = User.find_by_email(@user_mobile.email)
    @client = Client.find_by_user_id(@user)
    if @client
      @client_payment_histories = ClientPaymentHistory.where(:client_id => @client).order("payment_day DESC").all
      render_json(200,@client_payment_histories)
    else
      render_json(401,"Usuário não encontrado!")
    end
  end
end

def profile_measures
  if @user_mobile.valid_token?(@token, @client_id)
    @user = User.find_by_email(@user_mobile.email)
    @client = Client.find_by_user_id(@user)
    @client_measure_histories = ClientMeasureHistory.where(:client_id => @client).order("created_at DESC").all
    @measures = Array.new
    @client_measure_histories.each do |m|
      @measures.push(m.measure)
    end
    @response = {"measure_histories": @client_measure_histories, "measures": @measures}
    render_json(200,@measures)
  else
    render_json(401,"Usuário não encontrado!")
  end
end

def profile_training
  if @user_mobile.valid_token?(@token, @client_id)
    @user = User.find_by_email(@user_mobile.email)
    @client = Client.find_by_user_id(@user)
    @training_clients = TrainingClient.where(:client_id => @client).order("created_at ASC").all
    @trainings = Array.new
    @training_clients.each do |t|
      item = {"id": t.training.id, "coach": t.training.coach.name,"group": t.group, "turn": t.turn, "date": t.created_at, "equipament": t.training.equipament.name, "exercise": t.training.exercise.name, "number": t.training.number.number, "repetition": t.training.repetition.number, "muscle_type": t.training.muscle_type.name, "obs": t.training.how_make}
      @trainings.push(item)
    end
    render_json(200,@trainings)
  else
    render_json(401,"Usuário não encontrado!")
  end
end

  private

  def authenticate_user_mobile
    @user_mobile = UserMobile.find_by_email(request.headers['uid'])
    @client_id = request.headers['client']
    @token = request.headers['access-token']
  end

end
