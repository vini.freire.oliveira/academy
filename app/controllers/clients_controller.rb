class ClientsController < ApplicationController
  before_action :set_client, only: [:edit, :update, :destroy]
  before_action :client_block_acess, except: [:index]
  before_action :authenticate_user!



  # GET /clients
  # GET /clients.json
  def index
    if current_user
      @client = Client.find_by_user_id(current_user)
      if @client
        @training_clients = TrainingClient.where(:client_id => @client).order(:group).all
      end
    end
  end

  # GET /clients/1
  # GET /clients/1.json
  def show
    @user = User.find_by_email(params[:client][:email])
    @client = Client.find_by_user_id(@user)
    if @client
      @payments = ClientPaymentHistory.where(:client_id => @client).order('payment_day DESC').all
    else
      redirect_to "/manager_clients", alert: 'Aluno não encontrado!'
    end
  end

  # GET /clients/new
  def new
    @client = Client.new
    if current_user
      @admin = Admin.find_by_user_id(current_user)
      @manager = Manager.find_by_user_id(current_user)

      if @admin
        @gyms = Gym.order('name ASC').all
        @plan_clients = PlanClient.order('price DESC').all
      elsif @manager
        @gyms = Gym.order('name ASC').where(:manager_id => @manager).all
        @gyms.each do |gym|
          @plan_clients = PlanClient.order('price DESC').where(:gym_id => gym)
        end
      end
    end

  end

  # GET /clients/1/edit
  def edit
  end

  # POST /clients
  # POST /clients.json
  def create
    @user = User.new(user_params)

    if @user.save
      @password = params[:user][:password]
      @user_mobile = UserMobile.create(:email => @user.email, :password => @password, :password_confirmation => @password)
      gym_id = params[:client][:gym_id]
      plan_client_id = params[:client][:plan_client_id]
      address = params[:client][:address]
      phone = params[:client][:phone]
      name = params[:client][:name]
      cpf = params[:client][:cpf]

      @client = Client.new(:user_id => @user.id,:gym_id => gym_id, :plan_client_id => plan_client_id, :address => address, :phone => phone, :name => name, :cpf => cpf)
      if @client.save
        redirect_to home_path, notice: 'Aluno criado com sucesso'
      else
        redirect_to home_path, notice: 'Aluno não foi criado'
      end
    else
      redirect_to home_path, notice: 'Usuário não foi criado'
    end

  end

  # PATCH/PUT /clients/1
  # PATCH/PUT /clients/1.json
  def update
    respond_to do |format|
      if @client.update(client_params)
        format.html { redirect_to @client, notice: 'Client was successfully updated.' }
        format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    @client.destroy
    respond_to do |format|
      format.html { redirect_to clients_url, notice: 'Client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:client).permit(:user_id, :gym_id, :plan_client_id, :address, :phone, :name, :cpf)
    end

    def user_params
      params.require(:user).permit(:email,:password)
    end
end
