class NumbersController < ApplicationController
  before_action :set_number, only: [:show, :edit, :update, :destroy]
  before_action :client_block_acess, except: [:index]
  before_action :authenticate_user!

  # GET /numbers
  # GET /numbers.json
  def index
    if current_user
      @admin = Admin.find_by_user_id(current_user)
      @manager = Manager.find_by_user_id(current_user)
      @coach = Coach.find_by_user_id(current_user)
      @client = Client.find_by_user_id(current_user)

      if @admin
        @numbers = Number.all
      elsif @manager
        @gym = Gym.find_by_manager_id(@manager)
        @numbers = Number.where(:gym_id => @gym.id).order('number ASC').all
      elsif @coach
        @numbers = Number.where(:gym_id => @coach.gym_id).order('number ASC').all
      elsif @client
        @numbers = Number.where(:gym_id => @client.gym_id).order('number ASC').all
      end
    end
  end

  # GET /numbers/1
  # GET /numbers/1.json
  def show
  end

  # GET /numbers/new
  def new
    @number = Number.new
  end

  # GET /numbers/1/edit
  def edit
  end

  # POST /numbers
  # POST /numbers.json
  def create
    if current_user
      @manager = Manager.find_by_user_id(current_user)
      @coach = Coach.find_by_user_id(current_user)
      @number = params[:number][:number]

      if @manager
        @gym = Gym.find_by_manager_id(@manager)
        @number = Number.new(:gym_id => @gym.id, :number => @number)
        if @number.save
          redirect_to numbers_path, notice: 'Cadastrada com sucesso'
        else
          redirect_to numbers_path, notice: 'Não cadastrado'
        end
      elsif @coach
        @number = Number.new(:gym_id => @coach.gym_id, :number => @number)
        if @number.save
          redirect_to numbers_path, notice: 'Cadastrado com sucesso'
        else
          redirect_to numbers_path, notice: 'Não cadastrado'
        end
      end
    end
  end

  # PATCH/PUT /numbers/1
  # PATCH/PUT /numbers/1.json
  def update
    respond_to do |format|
      if @number.update(number_params)
        format.html { redirect_to @number, notice: 'Number was successfully updated.' }
        format.json { render :show, status: :ok, location: @number }
      else
        format.html { render :edit }
        format.json { render json: @number.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /numbers/1
  # DELETE /numbers/1.json
  def destroy
    @number.destroy
    respond_to do |format|
      format.html { redirect_to numbers_url, notice: 'Number was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_number
      @number = Number.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def number_params
      params.require(:number).permit(:gym_id, :number)
    end
end
