module ApplicationHelper

  def manager_block_acess
    @manager = Manager.find_by_user_id(current_user)
    @coach = Coach.find_by_user_id(current_user)
    @client = Client.find_by_user_id(current_user)

    if @manager
      redirect_to managers_path, notice: 'Você não tem permissão para acessar essa pagina!'
    elsif @coach
      redirect_to coaches_path, notice: 'Você não tem permissão para acessar essa pagina!'
    elsif @client
      redirect_to clients_path, notice: 'Você não tem permissão para acessar essa pagina!'
    end

  end


  def coach_block_acess
    @coach = Coach.find_by_user_id(current_user)
    @client = Client.find_by_user_id(current_user)

    if @coach
      redirect_to coaches_path, notice: 'Você não tem permissão para acessar essa pagina!'
    elsif @client
      redirect_to clients_path, notice: 'Você não tem permissão para acessar essa pagina!'
    end

  end

  def client_block_acess
    @client = Client.find_by_user_id(current_user)

    if @client
      redirect_to clients_path, notice: 'Você não tem permissão para acessar essa pagina!'
    end

  end

  def render_json(code,msg)
    render :json => {
     :status => code,
     :message => msg
    }.to_json
  end

end
