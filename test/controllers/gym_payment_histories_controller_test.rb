require 'test_helper'

class GymPaymentHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @gym_payment_history = gym_payment_histories(:one)
  end

  test "should get index" do
    get gym_payment_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_gym_payment_history_url
    assert_response :success
  end

  test "should create gym_payment_history" do
    assert_difference('GymPaymentHistory.count') do
      post gym_payment_histories_url, params: { gym_payment_history: { gym_id: @gym_payment_history.gym_id, payment_date: @gym_payment_history.payment_date, price: @gym_payment_history.price } }
    end

    assert_redirected_to gym_payment_history_url(GymPaymentHistory.last)
  end

  test "should show gym_payment_history" do
    get gym_payment_history_url(@gym_payment_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_gym_payment_history_url(@gym_payment_history)
    assert_response :success
  end

  test "should update gym_payment_history" do
    patch gym_payment_history_url(@gym_payment_history), params: { gym_payment_history: { gym_id: @gym_payment_history.gym_id, payment_date: @gym_payment_history.payment_date, price: @gym_payment_history.price } }
    assert_redirected_to gym_payment_history_url(@gym_payment_history)
  end

  test "should destroy gym_payment_history" do
    assert_difference('GymPaymentHistory.count', -1) do
      delete gym_payment_history_url(@gym_payment_history)
    end

    assert_redirected_to gym_payment_histories_url
  end
end
