require 'test_helper'

class ClientMeasureHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @client_measure_history = client_measure_histories(:one)
  end

  test "should get index" do
    get client_measure_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_client_measure_history_url
    assert_response :success
  end

  test "should create client_measure_history" do
    assert_difference('ClientMeasureHistory.count') do
      post client_measure_histories_url, params: { client_measure_history: { client_id: @client_measure_history.client_id, data_measures: @client_measure_history.data_measures, measures_id: @client_measure_history.measures_id } }
    end

    assert_redirected_to client_measure_history_url(ClientMeasureHistory.last)
  end

  test "should show client_measure_history" do
    get client_measure_history_url(@client_measure_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_client_measure_history_url(@client_measure_history)
    assert_response :success
  end

  test "should update client_measure_history" do
    patch client_measure_history_url(@client_measure_history), params: { client_measure_history: { client_id: @client_measure_history.client_id, data_measures: @client_measure_history.data_measures, measures_id: @client_measure_history.measures_id } }
    assert_redirected_to client_measure_history_url(@client_measure_history)
  end

  test "should destroy client_measure_history" do
    assert_difference('ClientMeasureHistory.count', -1) do
      delete client_measure_history_url(@client_measure_history)
    end

    assert_redirected_to client_measure_histories_url
  end
end
