require 'test_helper'

class PlanServiceManagersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @plan_service_manager = plan_service_managers(:one)
  end

  test "should get index" do
    get plan_service_managers_url
    assert_response :success
  end

  test "should get new" do
    get new_plan_service_manager_url
    assert_response :success
  end

  test "should create plan_service_manager" do
    assert_difference('PlanServiceManager.count') do
      post plan_service_managers_url, params: { plan_service_manager: { plan_manager_id: @plan_service_manager.plan_manager_id, service_manager_id: @plan_service_manager.service_manager_id } }
    end

    assert_redirected_to plan_service_manager_url(PlanServiceManager.last)
  end

  test "should show plan_service_manager" do
    get plan_service_manager_url(@plan_service_manager)
    assert_response :success
  end

  test "should get edit" do
    get edit_plan_service_manager_url(@plan_service_manager)
    assert_response :success
  end

  test "should update plan_service_manager" do
    patch plan_service_manager_url(@plan_service_manager), params: { plan_service_manager: { plan_manager_id: @plan_service_manager.plan_manager_id, service_manager_id: @plan_service_manager.service_manager_id } }
    assert_redirected_to plan_service_manager_url(@plan_service_manager)
  end

  test "should destroy plan_service_manager" do
    assert_difference('PlanServiceManager.count', -1) do
      delete plan_service_manager_url(@plan_service_manager)
    end

    assert_redirected_to plan_service_managers_url
  end
end
