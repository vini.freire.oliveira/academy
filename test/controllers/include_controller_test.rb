require 'test_helper'

class IncludeControllerTest < ActionDispatch::IntegrationTest
  test "should get DeviseTokenAuth::Concerns::SetUserByToken" do
    get include_DeviseTokenAuth::Concerns::SetUserByToken_url
    assert_response :success
  end

end
