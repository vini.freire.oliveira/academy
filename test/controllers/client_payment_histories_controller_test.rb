require 'test_helper'

class ClientPaymentHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @client_payment_history = client_payment_histories(:one)
  end

  test "should get index" do
    get client_payment_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_client_payment_history_url
    assert_response :success
  end

  test "should create client_payment_history" do
    assert_difference('ClientPaymentHistory.count') do
      post client_payment_histories_url, params: { client_payment_history: { client_id: @client_payment_history.client_id, payment_day: @client_payment_history.payment_day, price: @client_payment_history.price } }
    end

    assert_redirected_to client_payment_history_url(ClientPaymentHistory.last)
  end

  test "should show client_payment_history" do
    get client_payment_history_url(@client_payment_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_client_payment_history_url(@client_payment_history)
    assert_response :success
  end

  test "should update client_payment_history" do
    patch client_payment_history_url(@client_payment_history), params: { client_payment_history: { client_id: @client_payment_history.client_id, payment_day: @client_payment_history.payment_day, price: @client_payment_history.price } }
    assert_redirected_to client_payment_history_url(@client_payment_history)
  end

  test "should destroy client_payment_history" do
    assert_difference('ClientPaymentHistory.count', -1) do
      delete client_payment_history_url(@client_payment_history)
    end

    assert_redirected_to client_payment_histories_url
  end
end
