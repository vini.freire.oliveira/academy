require 'test_helper'

class TrainingClientsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @training_client = training_clients(:one)
  end

  test "should get index" do
    get training_clients_url
    assert_response :success
  end

  test "should get new" do
    get new_training_client_url
    assert_response :success
  end

  test "should create training_client" do
    assert_difference('TrainingClient.count') do
      post training_clients_url, params: { training_client: { client_id: @training_client.client_id, group: @training_client.group, training_id: @training_client.training_id, turn: @training_client.turn } }
    end

    assert_redirected_to training_client_url(TrainingClient.last)
  end

  test "should show training_client" do
    get training_client_url(@training_client)
    assert_response :success
  end

  test "should get edit" do
    get edit_training_client_url(@training_client)
    assert_response :success
  end

  test "should update training_client" do
    patch training_client_url(@training_client), params: { training_client: { client_id: @training_client.client_id, group: @training_client.group, training_id: @training_client.training_id, turn: @training_client.turn } }
    assert_redirected_to training_client_url(@training_client)
  end

  test "should destroy training_client" do
    assert_difference('TrainingClient.count', -1) do
      delete training_client_url(@training_client)
    end

    assert_redirected_to training_clients_url
  end
end
