require 'test_helper'

class PlanClientsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @plan_client = plan_clients(:one)
  end

  test "should get index" do
    get plan_clients_url
    assert_response :success
  end

  test "should get new" do
    get new_plan_client_url
    assert_response :success
  end

  test "should create plan_client" do
    assert_difference('PlanClient.count') do
      post plan_clients_url, params: { plan_client: { gym_id: @plan_client.gym_id, name: @plan_client.name, price: @plan_client.price } }
    end

    assert_redirected_to plan_client_url(PlanClient.last)
  end

  test "should show plan_client" do
    get plan_client_url(@plan_client)
    assert_response :success
  end

  test "should get edit" do
    get edit_plan_client_url(@plan_client)
    assert_response :success
  end

  test "should update plan_client" do
    patch plan_client_url(@plan_client), params: { plan_client: { gym_id: @plan_client.gym_id, name: @plan_client.name, price: @plan_client.price } }
    assert_redirected_to plan_client_url(@plan_client)
  end

  test "should destroy plan_client" do
    assert_difference('PlanClient.count', -1) do
      delete plan_client_url(@plan_client)
    end

    assert_redirected_to plan_clients_url
  end
end
