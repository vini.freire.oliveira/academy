require 'test_helper'

class PlanServiceClientsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @plan_service_client = plan_service_clients(:one)
  end

  test "should get index" do
    get plan_service_clients_url
    assert_response :success
  end

  test "should get new" do
    get new_plan_service_client_url
    assert_response :success
  end

  test "should create plan_service_client" do
    assert_difference('PlanServiceClient.count') do
      post plan_service_clients_url, params: { plan_service_client: { plan_client_id: @plan_service_client.plan_client_id, service_client_id: @plan_service_client.service_client_id } }
    end

    assert_redirected_to plan_service_client_url(PlanServiceClient.last)
  end

  test "should show plan_service_client" do
    get plan_service_client_url(@plan_service_client)
    assert_response :success
  end

  test "should get edit" do
    get edit_plan_service_client_url(@plan_service_client)
    assert_response :success
  end

  test "should update plan_service_client" do
    patch plan_service_client_url(@plan_service_client), params: { plan_service_client: { plan_client_id: @plan_service_client.plan_client_id, service_client_id: @plan_service_client.service_client_id } }
    assert_redirected_to plan_service_client_url(@plan_service_client)
  end

  test "should destroy plan_service_client" do
    assert_difference('PlanServiceClient.count', -1) do
      delete plan_service_client_url(@plan_service_client)
    end

    assert_redirected_to plan_service_clients_url
  end
end
