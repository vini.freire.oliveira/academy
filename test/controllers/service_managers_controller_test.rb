require 'test_helper'

class ServiceManagersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @service_manager = service_managers(:one)
  end

  test "should get index" do
    get service_managers_url
    assert_response :success
  end

  test "should get new" do
    get new_service_manager_url
    assert_response :success
  end

  test "should create service_manager" do
    assert_difference('ServiceManager.count') do
      post service_managers_url, params: { service_manager: { name: @service_manager.name, price: @service_manager.price } }
    end

    assert_redirected_to service_manager_url(ServiceManager.last)
  end

  test "should show service_manager" do
    get service_manager_url(@service_manager)
    assert_response :success
  end

  test "should get edit" do
    get edit_service_manager_url(@service_manager)
    assert_response :success
  end

  test "should update service_manager" do
    patch service_manager_url(@service_manager), params: { service_manager: { name: @service_manager.name, price: @service_manager.price } }
    assert_redirected_to service_manager_url(@service_manager)
  end

  test "should destroy service_manager" do
    assert_difference('ServiceManager.count', -1) do
      delete service_manager_url(@service_manager)
    end

    assert_redirected_to service_managers_url
  end
end
