require 'test_helper'

class PlanManagersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @plan_manager = plan_managers(:one)
  end

  test "should get index" do
    get plan_managers_url
    assert_response :success
  end

  test "should get new" do
    get new_plan_manager_url
    assert_response :success
  end

  test "should create plan_manager" do
    assert_difference('PlanManager.count') do
      post plan_managers_url, params: { plan_manager: { name: @plan_manager.name, price: @plan_manager.price } }
    end

    assert_redirected_to plan_manager_url(PlanManager.last)
  end

  test "should show plan_manager" do
    get plan_manager_url(@plan_manager)
    assert_response :success
  end

  test "should get edit" do
    get edit_plan_manager_url(@plan_manager)
    assert_response :success
  end

  test "should update plan_manager" do
    patch plan_manager_url(@plan_manager), params: { plan_manager: { name: @plan_manager.name, price: @plan_manager.price } }
    assert_redirected_to plan_manager_url(@plan_manager)
  end

  test "should destroy plan_manager" do
    assert_difference('PlanManager.count', -1) do
      delete plan_manager_url(@plan_manager)
    end

    assert_redirected_to plan_managers_url
  end
end
