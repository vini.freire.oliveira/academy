require 'test_helper'

class ServiceClientsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @service_client = service_clients(:one)
  end

  test "should get index" do
    get service_clients_url
    assert_response :success
  end

  test "should get new" do
    get new_service_client_url
    assert_response :success
  end

  test "should create service_client" do
    assert_difference('ServiceClient.count') do
      post service_clients_url, params: { service_client: { name: @service_client.name, price: @service_client.price } }
    end

    assert_redirected_to service_client_url(ServiceClient.last)
  end

  test "should show service_client" do
    get service_client_url(@service_client)
    assert_response :success
  end

  test "should get edit" do
    get edit_service_client_url(@service_client)
    assert_response :success
  end

  test "should update service_client" do
    patch service_client_url(@service_client), params: { service_client: { name: @service_client.name, price: @service_client.price } }
    assert_redirected_to service_client_url(@service_client)
  end

  test "should destroy service_client" do
    assert_difference('ServiceClient.count', -1) do
      delete service_client_url(@service_client)
    end

    assert_redirected_to service_clients_url
  end
end
