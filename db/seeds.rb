# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# @user = User.create(:email => 'marcos@email.com', :password => '123456', :password_confirmation => '123456')
# Admin.create(:name => 'Marcos Moreira', :user_id => @user.id)

#User.create(email: 'manager@email.com', encrypted_password: 'manager1234')

MuscleType.create([{ name: 'Abdomen' },{ name: 'Ante braço' },{ name: 'Bíceps' },{ name: 'Costas' },{ name: 'Coxa' },{ name: 'Lombar' },{ name: 'Ombros' },{ name: 'Peitoral' },{ name: 'Pernas' },{ name: 'Trapézio' },{ name: 'Tríceps' }])
