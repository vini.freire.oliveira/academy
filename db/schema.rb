# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180317211718) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_admins_on_user_id"
  end

  create_table "client_measure_histories", force: :cascade do |t|
    t.bigint "client_id"
    t.bigint "measure_id"
    t.date "data_measures"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_client_measure_histories_on_client_id"
    t.index ["measure_id"], name: "index_client_measure_histories_on_measure_id"
  end

  create_table "client_payment_histories", force: :cascade do |t|
    t.bigint "client_id"
    t.float "price"
    t.date "payment_day"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_client_payment_histories_on_client_id"
  end

  create_table "clients", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "gym_id"
    t.bigint "plan_client_id"
    t.string "address"
    t.string "phone"
    t.string "name"
    t.string "cpf"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gym_id"], name: "index_clients_on_gym_id"
    t.index ["plan_client_id"], name: "index_clients_on_plan_client_id"
    t.index ["user_id"], name: "index_clients_on_user_id"
  end

  create_table "coaches", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "gym_id"
    t.string "name"
    t.string "address"
    t.string "phone"
    t.string "cpf"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gym_id"], name: "index_coaches_on_gym_id"
    t.index ["user_id"], name: "index_coaches_on_user_id"
  end

  create_table "equipaments", force: :cascade do |t|
    t.bigint "gym_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gym_id"], name: "index_equipaments_on_gym_id"
  end

  create_table "exercises", force: :cascade do |t|
    t.bigint "gym_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gym_id"], name: "index_exercises_on_gym_id"
  end

  create_table "gym_payment_histories", force: :cascade do |t|
    t.bigint "gym_id"
    t.float "price"
    t.date "payment_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gym_id"], name: "index_gym_payment_histories_on_gym_id"
  end

  create_table "gyms", force: :cascade do |t|
    t.bigint "manager_id"
    t.bigint "plan_manager_id"
    t.string "cnpj"
    t.string "name"
    t.string "address"
    t.string "cep"
    t.string "city"
    t.string "state"
    t.string "phone"
    t.string "cellphone"
    t.string "lat"
    t.string "lon"
    t.date "due_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["manager_id"], name: "index_gyms_on_manager_id"
    t.index ["plan_manager_id"], name: "index_gyms_on_plan_manager_id"
  end

  create_table "managers", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_managers_on_user_id"
  end

  create_table "measures", force: :cascade do |t|
    t.float "weight"
    t.float "height"
    t.float "waist"
    t.float "pectoral"
    t.float "hip"
    t.float "arm_right"
    t.float "arm_left"
    t.float "thigh_right"
    t.float "thigh_left"
    t.float "calf_right"
    t.float "calf_left"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "muscle_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "numbers", force: :cascade do |t|
    t.bigint "gym_id"
    t.string "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gym_id"], name: "index_numbers_on_gym_id"
  end

  create_table "plan_clients", force: :cascade do |t|
    t.bigint "gym_id"
    t.string "name"
    t.float "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gym_id"], name: "index_plan_clients_on_gym_id"
  end

  create_table "plan_managers", force: :cascade do |t|
    t.string "name"
    t.float "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "plan_service_clients", force: :cascade do |t|
    t.bigint "plan_client_id"
    t.bigint "service_client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["plan_client_id"], name: "index_plan_service_clients_on_plan_client_id"
    t.index ["service_client_id"], name: "index_plan_service_clients_on_service_client_id"
  end

  create_table "plan_service_managers", force: :cascade do |t|
    t.bigint "plan_manager_id"
    t.bigint "service_manager_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["plan_manager_id"], name: "index_plan_service_managers_on_plan_manager_id"
    t.index ["service_manager_id"], name: "index_plan_service_managers_on_service_manager_id"
  end

  create_table "repetitions", force: :cascade do |t|
    t.bigint "gym_id"
    t.string "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gym_id"], name: "index_repetitions_on_gym_id"
  end

  create_table "service_clients", force: :cascade do |t|
    t.bigint "gym_id"
    t.string "name"
    t.float "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gym_id"], name: "index_service_clients_on_gym_id"
  end

  create_table "service_managers", force: :cascade do |t|
    t.string "name"
    t.float "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "training_clients", force: :cascade do |t|
    t.bigint "client_id"
    t.bigint "training_id"
    t.string "group"
    t.string "turn"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_training_clients_on_client_id"
    t.index ["training_id"], name: "index_training_clients_on_training_id"
  end

  create_table "trainings", force: :cascade do |t|
    t.bigint "coach_id"
    t.bigint "equipament_id"
    t.bigint "exercise_id"
    t.bigint "number_id"
    t.bigint "repetition_id"
    t.bigint "muscle_type_id"
    t.string "how_make"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["coach_id"], name: "index_trainings_on_coach_id"
    t.index ["equipament_id"], name: "index_trainings_on_equipament_id"
    t.index ["exercise_id"], name: "index_trainings_on_exercise_id"
    t.index ["muscle_type_id"], name: "index_trainings_on_muscle_type_id"
    t.index ["number_id"], name: "index_trainings_on_number_id"
    t.index ["repetition_id"], name: "index_trainings_on_repetition_id"
  end

  create_table "user_mobiles", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "nickname"
    t.string "image"
    t.string "email"
    t.text "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_user_mobiles_on_confirmation_token", unique: true
    t.index ["email"], name: "index_user_mobiles_on_email", unique: true
    t.index ["reset_password_token"], name: "index_user_mobiles_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_user_mobiles_on_uid_and_provider", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "admins", "users"
  add_foreign_key "client_measure_histories", "clients"
  add_foreign_key "client_measure_histories", "measures"
  add_foreign_key "client_payment_histories", "clients"
  add_foreign_key "clients", "gyms"
  add_foreign_key "clients", "plan_clients"
  add_foreign_key "clients", "users"
  add_foreign_key "coaches", "gyms"
  add_foreign_key "coaches", "users"
  add_foreign_key "equipaments", "gyms"
  add_foreign_key "exercises", "gyms"
  add_foreign_key "gym_payment_histories", "gyms"
  add_foreign_key "gyms", "managers"
  add_foreign_key "gyms", "plan_managers"
  add_foreign_key "managers", "users"
  add_foreign_key "numbers", "gyms"
  add_foreign_key "plan_clients", "gyms"
  add_foreign_key "plan_service_clients", "plan_clients"
  add_foreign_key "plan_service_clients", "service_clients"
  add_foreign_key "plan_service_managers", "plan_managers"
  add_foreign_key "plan_service_managers", "service_managers"
  add_foreign_key "repetitions", "gyms"
  add_foreign_key "service_clients", "gyms"
  add_foreign_key "training_clients", "clients"
  add_foreign_key "training_clients", "trainings"
  add_foreign_key "trainings", "coaches"
  add_foreign_key "trainings", "equipaments"
  add_foreign_key "trainings", "exercises"
  add_foreign_key "trainings", "muscle_types"
  add_foreign_key "trainings", "numbers"
  add_foreign_key "trainings", "repetitions"
end
