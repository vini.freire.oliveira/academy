class CreateClientMeasureHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :client_measure_histories do |t|
      t.references :client, foreign_key: true
      t.references :measure, foreign_key: true
      t.date :data_measures

      t.timestamps
    end
  end
end
