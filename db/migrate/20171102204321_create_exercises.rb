class CreateExercises < ActiveRecord::Migration[5.1]
  def change
    create_table :exercises do |t|
      t.references :gym, foreign_key: true
      t.string :name

      t.timestamps
    end
  end
end
