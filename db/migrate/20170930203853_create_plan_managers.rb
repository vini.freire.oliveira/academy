class CreatePlanManagers < ActiveRecord::Migration[5.1]
  def change
    create_table :plan_managers do |t|
      t.string :name
      t.float :price

      t.timestamps
    end
  end
end
