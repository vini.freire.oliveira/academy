class CreateEquipaments < ActiveRecord::Migration[5.1]
  def change
    create_table :equipaments do |t|
      t.references :gym, foreign_key: true
      t.string :name

      t.timestamps
    end
  end
end
