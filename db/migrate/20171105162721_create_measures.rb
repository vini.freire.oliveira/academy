class CreateMeasures < ActiveRecord::Migration[5.1]
  def change
    create_table :measures do |t|
      t.float :weight
      t.float :height
      t.float :waist
      t.float :pectoral
      t.float :hip
      t.float :arm_right
      t.float :arm_left
      t.float :thigh_right
      t.float :thigh_left
      t.float :calf_right
      t.float :calf_left

      t.timestamps
    end
  end
end
