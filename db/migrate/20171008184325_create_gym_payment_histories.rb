class CreateGymPaymentHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :gym_payment_histories do |t|
      t.references :gym, foreign_key: true
      t.float :price
      t.date :payment_date

      t.timestamps
    end
  end
end
