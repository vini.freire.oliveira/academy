class CreatePlanServiceManagers < ActiveRecord::Migration[5.1]
  def change
    create_table :plan_service_managers do |t|
      t.references :plan_manager, foreign_key: true
      t.references :service_manager, foreign_key: true

      t.timestamps
    end
  end
end
