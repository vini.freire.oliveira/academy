class CreateTrainingClients < ActiveRecord::Migration[5.1]
  def change
    create_table :training_clients do |t|
      t.references :client, foreign_key: true
      t.references :training, foreign_key: true
      t.string :group
      t.string :turn

      t.timestamps
    end
  end
end
