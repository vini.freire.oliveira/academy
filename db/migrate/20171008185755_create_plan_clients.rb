class CreatePlanClients < ActiveRecord::Migration[5.1]
  def change
    create_table :plan_clients do |t|
      t.references :gym, foreign_key: true
      t.string :name
      t.float :price

      t.timestamps
    end
  end
end
