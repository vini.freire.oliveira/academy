class CreatePlanServiceClients < ActiveRecord::Migration[5.1]
  def change
    create_table :plan_service_clients do |t|
      t.references :plan_client, foreign_key: true
      t.references :service_client, foreign_key: true

      t.timestamps
    end
  end
end
