class CreateClientPaymentHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :client_payment_histories do |t|
      t.references :client, foreign_key: true
      t.float :price
      t.date :payment_day

      t.timestamps
    end
  end
end
