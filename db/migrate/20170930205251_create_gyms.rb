class CreateGyms < ActiveRecord::Migration[5.1]
  def change
    create_table :gyms do |t|
      t.references :manager, foreign_key: true
      t.references :plan_manager, foreign_key: true
      t.string :cnpj
      t.string :name
      t.string :address
      t.string :cep
      t.string :city
      t.string :state
      t.string :phone
      t.string :cellphone
      t.string :lat
      t.string :lon
      t.date :due_date

      t.timestamps
    end
  end
end
