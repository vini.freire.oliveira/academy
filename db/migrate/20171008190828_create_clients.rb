class CreateClients < ActiveRecord::Migration[5.1]
  def change
    create_table :clients do |t|
      t.references :user, foreign_key: true
      t.references :gym, foreign_key: true
      t.references :plan_client, foreign_key: true
      t.string :address
      t.string :phone
      t.string :name
      t.string :cpf

      t.timestamps
    end
  end
end
