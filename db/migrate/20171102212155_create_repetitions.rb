class CreateRepetitions < ActiveRecord::Migration[5.1]
  def change
    create_table :repetitions do |t|
      t.references :gym, foreign_key: true
      t.string :number

      t.timestamps
    end
  end
end
