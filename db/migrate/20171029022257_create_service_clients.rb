class CreateServiceClients < ActiveRecord::Migration[5.1]
  def change
    create_table :service_clients do |t|
      t.references :gym, foreign_key: true
      t.string :name
      t.float :price

      t.timestamps
    end
  end
end
