class CreateCoaches < ActiveRecord::Migration[5.1]
  def change
    create_table :coaches do |t|
      t.references :user, foreign_key: true
      t.references :gym, foreign_key: true
      t.string :name
      t.string :address
      t.string :phone
      t.string :cpf

      t.timestamps
    end
  end
end
