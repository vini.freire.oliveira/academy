class CreateTrainings < ActiveRecord::Migration[5.1]
  def change
    create_table :trainings do |t|
      t.references :coach, foreign_key: true
      t.references :equipament, foreign_key: true
      t.references :exercise, foreign_key: true
      t.references :number, foreign_key: true
      t.references :repetition, foreign_key: true
      t.references :muscle_type, foreign_key: true
      t.string :how_make

      t.timestamps
    end
  end
end
