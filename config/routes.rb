Rails.application.routes.draw do
  resources :client_measure_histories
  resources :training_clients
  resources :repetitions
  resources :numbers
  resources :exercises
  resources :equipaments
  resources :client_payment_histories
  resources :clients
  #resources :plan_service_clients
  resources :service_clients
  resources :plan_clients
  resources :gym_payment_histories
  resources :coaches
  resources :gyms
  #resources :plan_service_managers
  resources :service_managers
  resources :plan_managers
  resources :managers
  resources :admins

  root to: 'application#home'

  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register', edit: 'settings' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/home', to: 'application#home', as: 'home'
  get '/secret', to: 'application#secret', as: 'secret'

  get '/manager_clients', to: 'managers#show_clients'
  get '/manager_coaches', to: 'managers#show_coaches'

  get '/admin_managers', to: 'admins#show_managers'
  get '/admin_managers/:email', to: 'admins#show_manager'
  get '/gym_payments/:id',  to: 'gyms#payments'

  # token auth routes available at /api_application/mobile/auth
  namespace :api_application do
    scope :mobile do
      mount_devise_token_auth_for 'UserMobile', at: 'auth'
    end
  end

  devise_scope :user_mobile do
    get 'api_application/mobile/mobile_profile', to: 'mobile#mobile_profile'
    get 'api_application/mobile/profile_payments', to: 'mobile#profile_payments'
    get 'api_application/mobile/profile_training', to: 'mobile#profile_training'
    get 'api_application/mobile/profile_measures', to: 'mobile#profile_measures'

  end

end
